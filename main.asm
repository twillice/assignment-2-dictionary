section .bss
%define BUFFER_SIZE 256
dict_key_buffer: resb 256

section .rodata
%include "words.inc"
key_not_found: db "Key not found.", 0
key_too_long: db "Key is too long. It must be less than 256 characters.", 0

section .text
%include "lib.inc"
%include "dict.inc"

global _start
_start:
    mov rdi, dict_key_buffer
    mov rsi, BUFFER_SIZE
    call read_string
    test rax, rax
    jz .key_too_long
    push rdx                        ; rdx - длина прочитанного ключа
    mov rdi, rax
    mov rsi, LAST_DICT_ENTRY
    call find_word
    test rax, rax
    jz .key_not_found
    pop rdx
    lea rdi, [rax+8+rdx+1]          ; получаем адрес значения, сместившись через указатель на предыдущую запись (+8) и строку ключа (rdx+1)
    call print_string
    jmp .exit
    .key_not_found:
        mov rdi, key_not_found
        call print_string_err
        jmp .exit
    .key_too_long:
        mov rdi, key_too_long
        call print_string_err
    .exit:
        xor rdi, rdi
        call exit