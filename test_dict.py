import subprocess
from subprocess import CompletedProcess

import pytest


def eval_program(key: str) -> CompletedProcess:
    return subprocess.run('./main', input=key, capture_output=True, text=True)


@pytest.mark.parametrize('key, value', [('key1', ''),
                                        ('another key', 'some value???'),
                                        ('uXr~4# R iii', ' '),
                                        ('Pretium quam vulputate dignissim suspendisse in est ante in nibh. Sagittis nisl rhoncus mattis rhoncus. In fermentum et sollicitudin ac orci phasellus egestas tellus. Duis at tellus at urna condimentum mattis pellentesque id. Diam vel quam elementum pulvi', 'fhhasb1212ifasbdfiaiua4124hsduiahdana979dsoashd63778iadia9sbdiabsduavdasid')])
def test_key_exists(key, value):
    assert eval_program(key).stdout == value


@pytest.mark.parametrize('key', ['key', 'k', '  key1 gg', ''])
def test_key_not_exists(key):
    assert eval_program(key).stderr == 'Key not found.'


@pytest.mark.parametrize('key', ['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed. Senectus et netus et malesuada. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Venenatis a condimentum vitae sapien pellentesque habitant.'])
def test_long_key(key):
    assert eval_program(key).stderr == 'Key is too long. It must be less than 256 characters.'
