main: lib.o dict.o main.o
	ld -o $@ $^

%.o: %.asm $(wildcard *.inc)
	nasm -felf64 $<

.PHONY: test
test:
	pytest --tb=short --disable-warnings

.PHONY: clean
clean:
	rm -f *.o main