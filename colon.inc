%define LAST_DICT_ENTRY 0
%macro colon 2
    %2: dq LAST_DICT_ENTRY
        db %1, 0
    %define LAST_DICT_ENTRY %2
%endmacro
