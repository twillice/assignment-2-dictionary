section .text
%include "lib.inc"

; Принимает в rdi указатель на нуль-терминированную строку - ключ, в rsi указатель на последнюю запись словаря.
; Проходит по словарю в поисках ключа.
; Возвращает в rax адрес записи словоря по ключу или 0, если запись не найдено.
global find_word
find_word:
    push r12
    push r13
    mov r12, rdi                ; r12 - указатель на строку искомого ключа
    mov r13, rsi                ; r13 - указатель на текущую запись словаря
    xor rax, rax
    .loop:
        test r13, r13
        jz .end
        lea rsi, [r13+8]        ; по адресу r13+8 находится строка ключа текущей записи
        mov rdi, r12
        call string_equals
        test rax, rax
        jnz .end
        mov r13, [r13]          ; смещение указателя на следующую запись
        jmp .loop
    .end:
        mov rax, r13
    pop r13
    pop r12
    ret
